FROM python:3.7

# Install pipenv
RUN pip install pipenv

# Add files
ADD . /app
WORKDIR /app

# Install dependencies
RUN pipenv install --system
